#!/usr/bin/env groovy

library identifier: 'jenkins-shared-library@main', retriever: modernSCM(
    [$class: 'GitSCMSource',
     remote: 'https://gitlab.com/kuanypeter/jenkins-shared-library.git',
     credentialsId: 'gitlab-credentials'
    ]
)

pipeline {
    agent any
    tools {
        maven 'maven'
    }
    // environment {
    //     IMAGE_NAME = 'cholkuany/devops:java-maven-app-1.0.0'
    // }
    stages {
        stage('increment version') {
            steps {
                script {
                    echo 'incrementing app version...'
                    sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }

        stage('build app') {
            steps {
               script {
                  echo 'building application jar...'
                  buildJar()
               }
            }
        }
        stage('build image') {
            steps {
                script {
                   echo 'building docker image...'
                   buildImage(env.IMAGE_NAME)
                   dockerLogin()
                   dockerPush(env.IMAGE_NAME)
                }
            }
        }
        stage('deploy') {
            steps {
                script {
                   echo 'deploying docker image to EC2...'

                   def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
                   def ec2Instance = "ec2-user@35.180.251.121"

                   def dockerCMD= "docker run -p 8080:8080 -d ${IMAGE_NAME}"

                   sshagent(['ec2-ssh-key']) {
                       sh "ssh -o StrictHostKeyChecking=no ec2-user@35.180.251.121 ${dockerCMD}"

                       sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
                       sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                       sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
                   }
                }
            }
        }

        stage('commit version update') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credential-username', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                        // git config here for the first time run
                        // sh 'git config --global user.email "jenkins@email.com"'
                        // sh 'git config --global user.name "jenkins"'

                        // sh 'git status'
                        // sh 'git branch'
                        // sh 'git config --list'

                        sh "git remote set-url origin https://${USERNAME}:${PASSWORD}@gitlab.com/kuanypeter/jenkins-java-maven-app.git"
                        sh 'git add .'
                        sh 'git commit -m "ci: version increment"'
                        sh 'git push origin HEAD:jenkins-jobs'
                    }
                }
            }
        }


    }
}
