### Demo Project:
Deploy Web Application on EC2 Instance (manually)

### Technologies used:
AWS, Docker, Linux

### Project Description:
- Create and configure an EC2 Instance on AWS
   - EC2 -> Launch an instance - give it a name
   - Choose OS image - choose instance type
   - Create key pair on AWS
   - Choose VPC and Subnet or leave default config
   - Configure security group
   - Configure storage
   - Launch instance
   - Store key pair pem in home directory .ssh and change permissions to chmod 400 .ssh/mykeypair.pem
   - ssh to the server - ssh -i ~/.ssh/mykeypair.pem ec2-user@instance_public_address
- Install Docker on remote EC2 Instance
   - Check package manage tool for updates - sudo yum update
   - sudo yum install docker
   - Start docker daemon - sudo service docker start 
   - add ec2-user to docker group - sudo usermod -aG docker $USER
   - exit the server to add the user and connect again
- Deploy Docker image from private Docker repository on EC2 Instance
   - docker login
   - Pull docker image
   - Run image e.g. docker run -d -p 8080:8080 imageName
   - Allow container port in security groups inbound rules
   - Access the app in the browser

----------

### Demo Project:
CD - Deploy Application from Jenkins Pipeline to EC2 Instance
(automatically with docker)

### Technologies used:
AWS, Jenkins, Docker, Linux, Git, Java, Maven, Docker Hub

### Project Description:
- Prepare AWS EC2 Instance for deployment (Install Docker)
  - Check package manage tool for updates - sudo yum update
  - sudo yum install docker
  - Start docker daemon - sudo service docker start 
  - add ec2-user to docker group - sudo usermod -aG docker $USER
  - exit the server to add the user and connect again
- Create ssh key credentials for EC2 server on Jenkins
  - install __ssh agent plugin__ in Jenkins UI
  - Configure EC2 credentials using the __key pair pem file__ downloaded from EC2 creation - SSH Username with private in Jenkins UI
- Extend the previous CI pipeline with deploy step to ssh into the remote EC2 instance and deploy newly built image from Jenkins server
  - Configure the credentials in Jenkinsfile. Use __Pipeline Syntax__ to generate code for ssh agent.
  - Use __-o StrictHostKeyChecking=no__ to suppress SSH pop-up
  - Log in to Docker repo on the EC2 server
  - In sshagent execute __ssh "-o StrictHostKeyChecking=no ec2-user@server_address docker run -p 3080:3080 -d cholkuany/devops:java-maven-app-1.0.0"__
- Configure security group on EC2 Instance to allow access to our web application
  - Allow Jenkins address to ssh to the server
  - Configure application port
  - Run Jenkins job and access the application from a browser

----------

### Demo Project:
CD - Deploy Application from Jenkins Pipeline on EC2
Instance (automatically with docker-compose)

### Technologies used:
AWS, Jenkins, Docker, Linux, Git, Java, Maven, Docker Hub

### Project Description:
- Install Docker Compose on AWS EC2 Instance
  - From this URL: https://gist.github.com/npearce/6f3c7826c7499587f00957fee62f8ee9
     Docker compose curl command: sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
  - sudo chmod +x /usr/local/bin/docker-compose - to run the binary
- Create docker-compose.yml file that deploys our web application image
  - Configure java-maven-app and postgres images
- Configure Jenkins pipeline to deploy newly built image using Docker Compose on EC2 server
  - In sshagent block(Jenkinsfile) copy docker-compose to ec2 instance 
     __sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ec2-user@server_address:/home/ec2-user"__
     __sh "ssh -o StrictHostKeyChecking=no ec2-user@server_address docker-compose.yaml up --detach"__
- Improvement: Extract multiple Linux commands that are executed on remote server into a separate shell script and execute the script from Jenkinsfile
 - Deploy stage: def shellCmd = "bash ./server-cmds.sh" 
 - server-cmds.sh
    sh "scp server-cmds.sh ec2-user@server_address:/home/ec2-user"
    sh "ssh -o StrictHostKeyChecking=no ec2-user@server_address ${shellCmd}"

----------
### Demo Project:
Complete the CI/CD Pipeline (Docker-Compose, Dynamic
versioning)

### Technologies used:
AWS, Jenkins, Docker, Linux, Git, Java, Maven, Docker Hub

### Project Description:
- CI step:Increment version
- CI step: Build artifact for Java Maven application
- CI step: Build and push Docker image to Docker Hub
- CD step: Deploy new application version with Docker Compose
- CD step: Commit the version update

----------

### Demo Project:
Interacting with AWS CLI

### Technologies used:
AWS, Linux

### Project Description:
- Install and configure AWS CLI tool to connect to our AWS account
 - brew install awscli
 - Configure aws access key, Secret access key, region and output format(json) - aws configure
- Create EC2 Instance using the AWS CLI with all necessary configurations like Security Group
 - aws ec2 run-instances \
    --image-id ami-xxxx \  - # aws ec2 describe-images
    --count 1 \
    --instance-type t2.micro \
    --key-name awscli-keypair \
    --security-group-ids awscli-sg-xxxx \ - # aws ec2 describe-security-groups
    --subnet-id subnet-xxxxx -  # aws ec2 describe-subnets
 - aws ec2 create-security-group
    --group-name awscli-sg
    --description "awscli security group" 
    --vpc-id vpc-xxxxx - use __aws ec2 describe-vpcs__ to get vpc-id
 - aws ec2 authorize-security-group-ingress - # Configure ssh rule in the firewall rules
    --group-id awscli-sg-xxxxxx
    --protocol tcp 
    --port 22 
    --cidr 120.100.133.11/32
 
- Create SSH key pair
 - aws ec2 create-key-pair
    --key-name awscli-keypair
    --query "KeyMaterial" 
    --output text > awscli-keypair.pem
 - chmod 400 awscli-keypair.pem
- Create IAM resources like User, Group, Policy using the AWS CLI
  - aws iam create-group --group-name awscliGroup
  - aws iam create-user --user-name awscliUser
  - aws iam add-user-to-group --user-name awscliUser --group-name awscliGroup
  - aws iam attach-group-policy --group-name awscliGroup --policy-arn arn:xxxxx - # aws iam list-policies --query 'Policies[?PolicyName==`AmazonEC2FullAccess`].Arn' --output text
  - aws iam create-login-profile --user-name awscliUser --password pass --password-reset-required
  - vim changePasswordPolicy.json - configure password change policy here
  - aws iam create-policy --policy-name passwordChange --policy-document file://changePasswordPolicy.json
  - aws iam attach-group-policy --group-name awscliGroup --policy-arn arn:xxxxx:policy/passwordChange
  --- Log in the user using the aws UI
  - aws iam create-access-key --user-name awscliUser
      Grab access key id and secret access key
- List and browse AWS resources using the AWS CLI

----------


Module 9: AWS Services